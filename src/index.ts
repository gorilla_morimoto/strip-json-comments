'use strict';
enum CommentT {
	single,
	multi
}

type State = {
	insideString: boolean;
	insideComment: boolean;
	commentType: CommentT | null;
};

const state: State = {
	insideString: false,
	insideComment: false,
	commentType: null
};

export default (str: string) => {
	let offset = 0;
	let ret = '';

	for (let i = 0; i < str.length; i++) {
		const currentChar = str[i];
		const nextChar = str[i + 1];
		const { insideString, insideComment, commentType } = state;

		// "foo" の foo の部分を読む
		if (!insideComment && currentChar === '"') {
			const escaped = str[i - 1] === '\\' && str[i - 2] !== '\\';
			if (!escaped) {
				state.insideString = !insideString;
			}
		}

		if (insideString) {
			continue;
		}

		// コメントの中じゃない && スペースの部分を読んでいる && 次から1行コメントがはじまるとき
		if (!insideComment && currentChar + nextChar === '//') {
			// いままで読み進めてきた部分をバッファに入れる
			ret += str.slice(offset, i);
			// コメントの開始位置をoffsetに入れる
			offset = i;
			state.commentType = CommentT.single;
			state.insideComment = true;
			i++;
			continue;
		}

		// 1行コメントの中 && 次に改行が来る
		if (
			insideComment &&
			commentType === CommentT.single &&
			currentChar + nextChar === '\r\n'
		) {
			i++;
			state.commentType = null;
			state.insideComment = false;
			offset = i;
			continue;
		}

		// 1行コメントの中 && 次に改行が来る
		if (
			insideComment &&
			commentType === CommentT.single &&
			currentChar === '\n'
		) {
			state.insideComment = false;
			offset = i;
			continue;
		}

		// コメントの中じゃない && 次から複数行コメントがはじまるとき
		if (!insideComment && currentChar + nextChar === '/*') {
			// いままで読み進めてきた部分をバッファに入れる
			ret += str.slice(offset, i);
			offset = i;
			state.insideComment = true;
			state.commentType = CommentT.multi;
			i++;
			continue;
		}

		// 複数行コメントの中 && 次で複数行コメントが終わる
		if (
			insideComment &&
			commentType === CommentT.multi &&
			currentChar + nextChar === '*/'
		) {
			i++;
			state.insideComment = false;
			state.commentType = null;
			offset = i + 1;
			continue;
		}
	}

	return ret + str.substr(offset);
};
